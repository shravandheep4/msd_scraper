import requests
import zipfile
import os
import io


def download_url(url):
    r = requests.get(url, stream=True)
    z = zipfile.ZipFile(io.BytesIO(r.content))
    z.extractall('drivers/')

    print('Extracting from {}'.format(url))


def get_binary():

    chromedriver_url = 'https://chromedriver.storage.googleapis.com/88.0.4324.96/chromedriver_linux64.zip'
    headless_chrome_url = 'https://github.com/adieuadieu/serverless-chrome/releases/download/v1.0.0-57/dev-headless-chromium-amazonlinux-2.zip'

    download_url(chromedriver_url)
    download_url(headless_chrome_url)

    os.chmod('chromedriver', 0o777)
    os.chmod('headless-chromium', 0o777)


if __name__ == '__main__':
    get_binary()