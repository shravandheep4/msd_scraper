# Scraping tools
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup as Soup

# Data pipelining
import boto3
from boto3.s3.transfer import S3Transfer
from botocore.client import ClientError
from io import StringIO
from utils.upload_to_s3 import upload_csv

# Scraper
from scraper import sites

# utils
import pandas as pd
import numpy as np

from datetime import datetime, date
import simplejson as sjson
import json
import requests
import time 
import sys
import os

# driver installation | one time use
import drivers

# Debugging purpose
from icecream import ic

# global
S3_BUCKET = 'msd-scrapper'
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SITES_CONFIG = os.path.join(BASE_DIR, 'sites.json')

def main():

    # Get the chromedriver and headless chromium
    # drivers.get_binary()
    
    # Get sites and run the code 
    with open(SITES_CONFIG) as content:
        websites = json.load(content)

    for wsite in websites:

        # Get website information
        client = wsite['client']
        base_url = wsite['base_url']
        queries = wsite['queries']

        scrolls = wsite['scrolls']
        page_init = wsite['page_init']

        
        # Create instance of the site class
        scraper = getattr(sites, client)(client, base_url)

        # Scrape data
        print('Scraping',client)

        df = pd.DataFrame()

        for query in queries:
            df_q = scraper.scrape(query, scrolls, page_init)
            pd.concat([df, df_q], ignore_index=True)

        # Add timestamp
        today = date.today()
        created_at = today.strftime("%Y-%m-%d %H:%M:%S")
        df['created_at'] = created_at

        # Save and upload file to s3
        curr_time = datetime.now()
        filename = '{}_scraped_data_{}_structured.csv'.format(client, curr_time)
        s3_dir_path = 's3_to_redshift/{}'.format(filename)

        # upload_csv(df, S3_BUCKET, s3_dir_path)

        # df.to_csv('./testing.csv', index=False)

if __name__ == '__main__':
    main()
