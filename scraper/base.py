# Scraping tools
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup as Soup

from os.path import join, dirname, abspath
import simplejson as sjson
import time
import sys

import pandas as pd
import numpy as np

from datetime import datetime
from icecream import ic

# Globals
BASE_DIR = dirname(dirname(abspath(__file__)))
DRIVER_DIR = join(BASE_DIR, 'drivers/')

class Scraper():
    
    def __init__(self, client, base_url):
        self.client = client.lower()
        self.base_url = base_url

    def get_product_listing(self, query : str, scrolls : int) -> list:
        pass

    def fetch_data(self, product_listing : list) -> list:
        pass

    def scrape(self, query : str, scrolls : int, page_init : int) -> pd.DataFrame:

        product_listing = self.get_product_listing(query, scrolls, page_init)
        data = self.fetch_data(product_listing)
        df = self.create_df(data)
        formatted_df = self.format_meta(df, self.client)

        return formatted_df

    @staticmethod
    def get_browser_driver():
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--ignore-certificate-errors')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--hide-scrollbars')
        chrome_options.binary_location = join(DRIVER_DIR, 'headless-chromium')

        # return webdriver.Chrome(join(DRIVER_DIR,'chromedriver'), options=chrome_options)
        return webdriver.Chrome(options=chrome_options)


    @staticmethod
    def create_df(data : list) -> pd.DataFrame:

        df = pd.DataFrame()
        
        for idx ,row in enumerate(data):
            for (k, v) in row.items():
                df.at[idx, k] = v

        return df

    @staticmethod
    def format_meta(df : pd.DataFrame, client) -> pd.DataFrame:

        formatted_df = pd.DataFrame()

        for idx, row in df.iterrows():

            meta = dict(row)
            product_url = meta.pop('product_url')

            formatted_df.at[idx, 'product_url'] = product_url
            formatted_df.at[idx, 'client'] = client
            formatted_df.at[idx, 'meta'] = sjson.dumps(meta, ignore_nan=True)

        return formatted_df
    