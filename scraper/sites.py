# Base class
from scraper.base import Scraper

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup as Soup

import pandas as pd
import numpy as np

from os.path import join, dirname, abspath
import time
import sys

from tqdm import tqdm
from datetime import datetime

# inherit from base class
class Depop(Scraper):

    def __init__(self, client, base_url):
        super().__init__(client, base_url)
        self.client = client.lower()
        self.base_url = base_url

    def get_product_listing(self, query : str, scrolls : int, page_init : int) -> list:

        base_url = self.base_url
        driver = self.get_browser_driver()

        driver.maximize_window()
        driver.get(base_url + query)

        product_listing = []

        options = driver.find_element_by_xpath("//select[@name='select__location']")
        options.click()
        number_of_options = len(options.find_elements_by_tag_name('option'))

        for op in range(number_of_options):
            
            options = driver.find_element_by_xpath("//select[@name='select__location']")
            options.click()

            options.find_elements_by_tag_name('option')[op].click()
            time.sleep(0.3)

            for i in range(scrolls):

                #### Keeps scrolling
                elements = driver.find_elements_by_xpath("//a[@data-testid='product__item']")
                element = elements[-1]
                element.location_once_scrolled_into_view

                time.sleep(1)


            page_content = driver.page_source
            soup = Soup(page_content, 'lxml')
            images = soup.find_all('ul', {'data-testid' : 'product__items'}) 
            
            if len(images) == 0: 
                raise Exception('Something went wrong. Could not find any listing')

            links = images[0].find_all('li')            

            for link in links:
                product_link = base_url[:-1] + link.find('a').get('href')
                product_listing.append(product_link)

        driver.close()
        return product_listing
        links = images[0].find_all('li')            

        for link in links:
            product_link = base_url[:-1] + link.find('a').get('href')
            product_listing.append(product_link)

        driver.close()
        return product_listing

    def fetch_data(self, product_listing):
        
        driver = self.get_browser_driver()
        driver.maximize_window()

        data = []

        with tqdm(total=len(product_listing)) as pbar:
            for idx,link in enumerate(product_listing):

                driver.get(link)
                time.sleep(0.2)

                try:

                    elements = driver.find_elements_by_xpath("//ul[@data-testid='product__items']")
                    element = elements[-1]
                    element.location_once_scrolled_into_view       
                    time.sleep(0.2)

                    page_content = driver.page_source
                    new = Soup(page_content, 'lxml')

                    other_details = new.find('div', {'data-testid' :'product__details'}).find('table')
                    
                    d_colour = other_details.find('td', {'data-testid' :'product__colour'})
                    d_style = other_details.find('td', {'data-testid' :'product__style'})
                    d_brand = other_details.find('td', {'data-testid' :'product__brand'})
                    d_cond = other_details.find('td', {'data-testid' :'product__condition'})
                    d_size = other_details.find('td', {'data-testid' :'product__size'})
                    d_price = other_details.find('td', {'data-testid' :'product__price'})

                    time_uploaded = new.find('div', {'data-testid' :'product__details'}).find('time')['datetime']

                    description = new.find('p', {'data-testid' : 'product__description'}).text
                    thumb_img = new.find('div', {'data-testid' : 'product__images'}).find_all('img')

                    similar_products = [x.get('src') for x in new.find('ul', {'data-testid' : 'product__items'}).find_all('img')]

                    thumb_data = {}
                    p_id = None

                    for index, image in enumerate(thumb_img):
                        src = image.get("src")
                        alt = image.get("alt")
                        
                        p_url = f'product_image_url_{index}'
                        p_alt = f'product_alt_des_{index}'

                        thumb_data[p_url] = src if src is not None else ''
                        thumb_data[p_alt] = alt if alt is not None else '' 

                        if p_id is None:
                            p_id = src.split('/')[-3]

                except Exception as e:
                    pbar.update(1)
                    continue

                data.append({
                    **thumb_data,
                    "description" : description, 
                    "colour" : str(d_colour.text) if d_colour is not None else '',
                    "brand" : str(d_brand.text) if d_brand is not None else '',
                    "style" : str(d_style.text) if d_style is not None else '',
                    "size" : str(d_size.text) if d_size is not None else '',
                    "price" : str(d_price.text) if d_price is not None else '',
                    "condition" : str(d_cond.text) if d_cond is not None else '',
                    'product_url' : link,
                    "product_id" : p_id,
                    "time_uploaded" : time_uploaded,
                    "similar_products" : str(similar_products)
                    })

                pbar.update(1)

        return data

class Babycouture(Scraper):

    def __init__(self, client, base_url):
        super().__init__(client, base_url)
        self.client = client.lower()
        self.base_url = base_url

    def get_product_listing(self, query : str, scrolls : int, start_page : int) -> list:

        base_url = self.base_url
        driver = self.get_browser_driver()
        driver.maximize_window()

        end_page = start_page + scrolls
        product_listing = []

        for i in range(start_page, end_page):

            driver.get(base_url + query + '?p={}'.format(i))

            page_content = driver.page_source
            soup = Soup(page_content, 'lxml')

            images = soup.find_all('div', {'class' : 'product-item-img'}) 
            
            if len(images) == 0: 
                raise Exception('Something went wrong. Could not find any listing')

            for image in images:
                product_link = image.find('a').get('href')
                product_listing.append(product_link)

            time.sleep(1)
            

        driver.close()
        return product_listing

    def fetch_data(self, product_listing):
        
        driver = self.get_browser_driver()
        driver.maximize_window()

        data = []

        with tqdm(total=len(product_listing)) as pbar:
            for idx,link in enumerate(product_listing):

                driver.get(link)
                time.sleep(0.2)

                try:
                    
                    elements = driver.find_elements_by_xpath("//div[@class='fotorama__stage__frame']")
                    element = elements[0]
                    element.click()      
                    time.sleep(0.05)

                    page_content = driver.page_source
                    new = Soup(page_content, 'lxml')

                    other_details = new.find('div', {'data-testid' :'product__details'}).find('table')
                    
                    d_colour = other_details.find('td', {'data-testid' :'product__colour'})
                    d_style = other_details.find('td', {'data-testid' :'product__style'})
                    d_brand = other_details.find('td', {'data-testid' :'product__brand'})
                    d_cond = other_details.find('td', {'data-testid' :'product__condition'})
                    d_size = other_details.find('td', {'data-testid' :'product__size'})
                    d_price = other_details.find('td', {'data-testid' :'product__price'})

                    time_uploaded = new.find('div', {'data-testid' :'product__details'}).find('time')['datetime']

                    description = new.find('p', {'data-testid' : 'product__description'}).text
                    thumb_img = new.find('div', {'data-testid' : 'product__images'}).find_all('img')

                    similar_products = [x.get('src') for x in new.find('ul', {'data-testid' : 'product__items'}).find_all('img')]

                    thumb_data = {}
                    p_id = None

                    for index, image in enumerate(thumb_img):
                        src = image.get("src")
                        alt = image.get("alt")
                        
                        p_url = f'product_image_url_{index}'
                        p_alt = f'product_alt_des_{index}'

                        thumb_data[p_url] = src if src is not None else ''
                        thumb_data[p_alt] = alt if alt is not None else '' 

                        if p_id is None:
                            p_id = src.split('/')[-3]

                except Exception as e:
                    pbar.update(1)
                    continue

                data.append({
                    **thumb_data,
                    "description" : description, 
                    "colour" : str(d_colour.text) if d_colour is not None else '',
                    "brand" : str(d_brand.text) if d_brand is not None else '',
                    "style" : str(d_style.text) if d_style is not None else '',
                    "size" : str(d_size.text) if d_size is not None else '',
                    "price" : str(d_price.text) if d_price is not None else '',
                    "condition" : str(d_cond.text) if d_cond is not None else '',
                    'product_url' : link,
                    "product_id" : p_id,
                    "time_uploaded" : time_uploaded,
                    "similar_products" : str(similar_products)
                    })

                pbar.update(1)

        return data

class Littletags(Scraper):

    def __init__(self, client, base_url):
        super().__init__(client, base_url)
        self.client = client.lower()
        self.base_url = base_url

    def get_product_listing(self, query : str, scrolls : int, start_page : int) -> list:

        base_url = self.base_url
        driver = self.get_browser_driver()
        driver.maximize_window()

        end_page = start_page + scrolls
        product_listing = []

        for i in range(start_page, end_page):

            driver.get(base_url + query + '?p={}'.format(i))
            time.sleep(1)

            page_content = driver.page_source
            soup = Soup(page_content, 'lxml')

            images = soup.find_all('a', {'class' : 'product-image'}) 
            
            if len(images) == 0: 
                raise Exception('Something went wrong. Could not find any listing')

            for image in images:
                product_link = image.get('href')
                product_listing.append(product_link)

            time.sleep(0.5)
            
        driver.close()
        return product_listing

    def fetch_data(self, product_listing):
        
        driver = self.get_browser_driver()
        driver.maximize_window()

        data = []

        with tqdm(total=len(product_listing)) as pbar:
            for idx,link in enumerate(product_listing):

                driver.get(link)
                time.sleep(0.2)

                try:
                    
                    elements = driver.find_elements_by_xpath("//div[@class='selectors']")[0]
                    selectors = elements.find_elements_by_tag_name("a")
                    time.sleep(0.05)

                    for e in selectors:
                        e.click()      
                        time.sleep(0.05)

                        page_content = driver.page_source
                        new = Soup(page_content, 'lxml')

                        image_url = new.find('div', {'class' :'app-figure'}).find('a').get('href')
                        description = "new.find('div', {'class' : 'description-text'}).find('span').text"

                        print(image_url)

                        data.append({
                            "image_url" : image_url,
                            "description" : description, 
                            'product_url' : link,
                            })

                except Exception as e:
                    print(e, end='\n\n')
                    pbar.update(1)
                    continue


                pbar.update(1)

        return data
