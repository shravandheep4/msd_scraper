import psycopg2
import paramiko

from sshtunnel import SSHTunnelForwarder
from psycopg2.extras import execute_batch

import boto3
from boto3.s3.transfer import S3Transfer
from io import StringIO

import numpy as np
import pandas as pd

from datetime import datetime
import simplejson as sjson
import argparse
import json
import os
import sys

def get_private_key(path):
    return paramiko.RSAKey.from_private_key_file(path)

def connect_to_db(user, pwd):

    pkey_path = 'pem/tunneling.pem'
    host_ip = '54.244.17.221'
    username = 'ubuntu'
    local_port = 15439
    db_port = 5439
    host_main = 'optimizedcluster.czunimvu3pdw.us-west-2.redshift.amazonaws.com'

    pkey = get_private_key(pkey_path)

    try:

        tunnel =  SSHTunnelForwarder(
            (host_ip, 22),
            ssh_username = username,
            ssh_pkey = pkey,
            remote_bind_address=(host_main, db_port),
            local_bind_address=('localhost', local_port))

        tunnel.start()
    
    except Exception as e:
        print("Couldn't tunnel.\n{}".format(e), end='\n\n')

    print('Tunneling', end='\n\n')

    dbname = 'msd'
    host = 'localhost'
    port = str(tunnel.local_bind_port)

    try:
        conn = psycopg2.connect(
            dbname= dbname,
            host = host, 
            port= port, 
            user= user, 
            password= pwd, 
        )
    except Exception as e:
        print("Couldn't connect to the database. \n {}".format(e), end='\n\n')

    print('Connecting to DB')
    return conn, tunnel

def stop_connection(conn, tunnel):

    print("Closing the connection and tunneling...")    
    
    conn.close()
    tunnel.stop()

def get_data(conn):

    table = 'scraped_data'
    schema = 'tag_data_combined'
    limit = None

    cursor = conn.cursor()
    query = "select * FROM {}.{}".format(schema, table)
    query += f' limit {limit};' if limit is not None else ';'

    try:
        cursor.execute(query)
        data = cursor.fetchall() 
    except Exception as e:
        print(e)
        sys.exit(1)

    columns_query = """select column_name from information_schema.columns where table_schema = '{}' and table_name='{}' order by ordinal_position;""".format(schema, table)
    
    try:
        cursor.execute(columns_query)
        column_names = [row[0] for row in cursor]
    except Exception as e:
        print(e)
        sys.exit(1)

    cursor.close()

    df = pd.DataFrame(data, columns=column_names)
    return df

def unpack_data(df):

    for idx, row in df.iterrows():
        meta = row['meta']
        meta_dict = sjson.loads(meta)

        for k,v in meta_dict.items():
            df.at[idx, k] = v if v is not None else ''

    df.drop(columns='meta', inplace=True)

    return df
        
def fetch_from_redshift(args):

    # Establish connection and tunneling

    path = args['path']
    user = args['user']
    pwd = args['pwd']

    conn, tunnel = connect_to_db(user, pwd)
    
    # Prepare data
    df = get_data(conn)
    df = unpack_data(df)

    stop_connection(conn, tunnel)

    print(len(df), 'data points fetched. Saving the csv')

    file_name = os.path.join(path,'formatted_scraped_data_{}.csv'.format(datetime.now()))
    df.to_csv(file_name, index=False)

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Fetch data from Redshift')
    parser.add_argument('--path', help = 'Path to store the data')
    parser.add_argument('--user', help = 'DB user')
    parser.add_argument('--pwd', help = 'DB pwd')

    args = parser.parse_args()
    args_dict = args.__dict__

    fetch_from_redshift(args_dict)