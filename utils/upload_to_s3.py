import boto3
from boto3.s3.transfer import S3Transfer
from botocore.client import ClientError

from io import StringIO

import pandas as pd

def upload_csv(df, bucket, s3_path):
    
    # session = boto3.Session( aws_access_key_id=<AWS_ACCESS_KEY_ID>, aws_secret_access_key=<AWS_SECRET_ACCESS_KEY>, ) 
    # s3_resource = session.resource()

    csv_buffer = StringIO()
    df.to_csv(csv_buffer, index=False, header=False)

    try:
        s3_resource = boto3.resource('s3')
        s3_resource.meta.client.head_bucket(Bucket=bucket)
        print('Bucket is present')
    except ClientError as e:
        print(e)

    try:
        s3_resource.Object(bucket, s3_path).put(Body=csv_buffer.getvalue())
        print('Uploaded to S3://{}'.format(bucket))
    except Exception as e:
        print('Failed to upload the csv to S3\n', e)
        return False

    return True

